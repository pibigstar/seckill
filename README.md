# 秒杀系统的实现

## 使用springBoot+mybatis实现

###主要使用技术

**后端部分：**

- [x] springboot整合mybatis
- [x] mybatis注解查询
- [x] mybatis联合查询
- [x] 事务操作

**JS部分**
- [x] js模块化写法
- [x] jquery-cookie用法
- [x] jquery-countdown用法